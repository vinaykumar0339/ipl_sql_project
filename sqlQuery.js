const con = require('./dbConfig');


// create database
function createDatabase(databaseName) {
    let sql = `CREATE DATABASE IF NOT EXISTS ${databaseName}`;
    con.query(sql, (err, result, fields) => {
        if (err) {
            console.log(err);
        }
        console.log(`Database ${databaseName} is created`);
    })
}

// create matches table
function createMatchesTable() {
    let sql = `CREATE TABLE IF NOT EXISTS matches (
        id INT NOT NULL AUTO_INCREMENT,
        season YEAR,
        city VARCHAR(30),
        date DATE,
        team1 VARCHAR(70),
        team2 VARCHAR(70),
        toss_winner VARCHAR(70),
        toss_decision VARCHAR(70),
        result VARCHAR(30),
        dl_applied VARCHAR(70),
        winner VARCHAR(70),
        win_by_runs INT,
        win_by_wickets INT,
        player_of_match VARCHAR(50),
        venue VARCHAR(100),
        umpire1 VARCHAR(30),
        umpire2 VARCHAR(30),
        umpire3 VARCHAR(30),
        PRIMARY KEY (id)
    )`;


    con.query(sql, (err, result, field) => {
        if (err) {
            console.log(err);
        }
        console.log("created matches table");
    })

}

function createDeliveriesTable() {
    let sql = `CREATE TABLE IF NOT EXISTS deliveries (
        id INT NOT NULL AUTO_INCREMENT,
        match_id INT,
        inning INT,
        batting_team VARCHAR(70),
        bowling_team VARCHAR(70),
        overs INT,
        ball INT,
        batsman VARCHAR(50),
        non_striker VARCHAR(50),
        bowler VARCHAR(50),
        is_super_over INT,
        wide_runs INT,
        bye_runs INT,
        legbye_runs INT,
        noball_runs INT,
        penalty_runs INT,
        batsman_runs INT,
        extra_runs INT,
        total_runs INT,
        player_dismissed VARCHAR(50),
        dismissal_kind VARCHAR(50),
        fielder VARCHAR(50),
        FOREIGN KEY (match_id) REFERENCES matches(id),
        PRIMARY KEY (id)
    )`;

    con.query(sql, (err, result, field) => {
        if (err) {
            console.log(err);
        }
        console.log('deliveries table is created');
    })
}

function insertInto(table, data) {
    let keys = Object.keys(data[0]);
    let insertData = data.map(obj => keys.map(key => obj[key]))
    let sql = `INSERT INTO ${table} (${keys.join(',')}) values ?`;
    con.query(sql, [insertData], (err, result, field) => {
        if (err) {
            console.log(err);
        }

        console.log(`data added into table ${table}`);
    })
}


module.exports = {
    createDatabase,
    createMatchesTable,
    insertInto,
    createDeliveriesTable
}