const { createDatabase, createMatchesTable, createDeliveriesTable, insertInto } = require('./sqlQuery');
const csv = require('csvtojson');


// createDatabase('ipl_sql_project');
// createMatchesTable();
// createDeliveriesTable();

// insert data into matches table

function insertDataIntoDatabase(path, tableName) {
    csv()
        .fromFile(path)
        .then(data => insertInto(tableName, data))
}

insertDataIntoDatabase('./data/matches.csv', 'matches');
insertDataIntoDatabase('./data/deliveries.csv', 'deliveries');